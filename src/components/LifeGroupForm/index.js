import React from 'react';
import { Modal, Row, Col, Form } from 'react-bootstrap';
import { Button } from '../ButtonElements';

const LifeGroupForm = (props) => {
  return (
    <Modal
    {...props}
    size="lg"
    aria-labelledby="contained-modal-title-vcenter"
    centered
    className="give-contents"
  >
    <Modal.Header className="give-heading2" closeButton>
      <Modal.Title id="contained-modal-title-vcenter">
        Welcome to our LifeGroup
      </Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <Form action="mailto:krishamayramos22@gmail.com" method=""> {/* onSubmit={handleSubmit}>*/}
        <Row>
          <Col xs={6} sm={6} lg={6} className="form-name">
              {['radio'].map((type) => (
                <div key={`inline-${type}`} className="mb-3">
                  <Form.Check inline label="Male" type={type} id={`inline-${type}-Male`} />
                  <Form.Check inline label="Female" type={type} id={`inline-${type}-Female`} />
                </div>
              ))}
          </Col>

          <Col xs={6} sm={6} lg={6} className="form-name">
            <Form.Group controlId="" className="give-form-label">
              <Form.Control as="select" className="input-area">
                <option>Select status</option>
                <option>Single</option>
                <option>Married</option>
                <option>Divorced/Separated</option>
                <option>Widow/Widower</option>
              </Form.Control>                  
            </Form.Group>
          </Col>

          <Col lg={6} className="form-name">
            <Form.Group className="form-label">
              Date of Birth
              <Form.Control className="input-area" type="month" name="birthday" required/>
            </Form.Group>
          </Col>   

          <Col lg={6} className="form-name">
            <Form.Group className="form-label">
              Occupation <Form.Text className="text-muted">
              &#40;optional&#41;
              </Form.Text>
              <Form.Control className="input-area" type="text" name="occupation" placeholder="" />
            </Form.Group>
          </Col>   

          <Col lg={6} className="form-name">
            <Form.Group className="form-label">
              First Name
              <Form.Control className="input-area" type="text" name="firstName" placeholder="John" required/>
            </Form.Group>
          </Col>

          <Col lg={6} className="form-name">
            <Form.Group className="form-label">
              Last Name
              <Form.Control className="input-area" type="text" name="lastName" placeholder="Doe" required/>
            </Form.Group>
          </Col>



          <Col lg={6}>
            <Form.Group className="form-label">      
              Email Address
              <Form.Control className="input-area" type="email" name="email" placeholder="email@example.com" required/>
            </Form.Group>            
          </Col>            

          <Col lg={6}>
            <Form.Group className="give-form-label">      
              Contact Number  
              <Form.Control className="input-area" type="tel" name="email" placeholder="08012345678" required/>
            </Form.Group>            
          </Col>

          <Col lg={12}>      
            <Form.Group>      
              <Button type="submit" className="submitButton" /*onClick={thankYou}*/>Join Now</Button>
            </Form.Group>                     
          </Col>     
        </Row>            
      </Form>
    </Modal.Body>
  </Modal>
  )
}

export default LifeGroupForm;
