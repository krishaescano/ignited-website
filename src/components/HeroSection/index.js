import React, { useState } from 'react';
import Video from '../../assets/videos/ignited.m4v';
import { HeroContainer, HeroBg, VideoBg, HeroContent, HeroH1, HeroBtnWrapper, ArrowForward, ArrowRight, WhoBtn } from './HeroElements';
import { Carousel, Row, Col } from 'react-bootstrap';
import image1 from '../../assets/images/activity.jpeg';
import image2 from '../../assets/images/activity1.jpeg';
import image3 from '../../assets/images/activity2.jpeg';
import image4 from '../../assets/images/activity3.jpeg';
import { Button } from '../ButtonElements';
import LifeGroupForm from '../LifeGroupForm';

const HeroSection = () => {
  const [ hover, setHover ] = useState(false);
  const [ showButton, setShowButton ] = useState(false);

  const onHover = () => {
    setHover(!hover)
  }

  return (
    <>
    <HeroContainer id='home'>
      <HeroBg>
        <VideoBg autoPlay loop muted src={Video} type='video/mp4' />
        {/* <video src={Video} muted loop autoPlay type='video/mp4' alt="ignited-church-logo"></video> */}
      </HeroBg>

      <HeroContent>
        <HeroH1> Welcome to the Family! </HeroH1>
        <HeroBtnWrapper>
          <WhoBtn href='/about' onMouseEnter={onHover} onMouseLeave={onHover} primary='true' dark='true'>
          Who We Are { hover ? <ArrowForward /> : <ArrowRight />}
          </WhoBtn>
{/*         
          <WatchBtn to='/watch' onMouseEnter={onHover} onMouseLeave={onHover}>
          Watch { hover ? <ArrowForward /> : <ArrowRight />}
          </WatchBtn> */}
        </HeroBtnWrapper>
      </HeroContent>
    </HeroContainer>

    <section className="church-verse">
      <div>
        <h3 className="church-verse-h3">We're glad to welcome you here in our family!</h3>
        <p>"Therefore welcome one another as Christ has welcomed you, for the glory of God"
        <p className="church-verse-p2">- Romans 15:7</p>
        </p>
      </div>
    </section>

    <Carousel className="church-activities" fade={true} pause={false}>
      <Carousel.Item interval={4000}>
        <img
          className="home-activities"
          src={image1}
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={2000}>
        <img
          className="home-activities"
          src={image2}
          alt="Third slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={2000}>
        <img
          className="home-activities"
          src={image3}
          alt="Third slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={2000}>
        <img
          className="home-activities"
          src={image4}
          alt="Fourth slide"
        />
      </Carousel.Item>
    </Carousel>

    <section className="church-verse">
      <div>
        <h3 className="church-verse-h3">Join a Lifegroup</h3>
        <p>Lifegroup is a small group of people desiring to meet under the supervision of church leadership for the purpose of growing in Christ through fellowship, spiritual encouragement and wholesome and edifying activities.</p>
        <Row>
          <Col sm={12} lg={6}>
            <div className="young">
              <h5 className="lifegroup-header">Who can join a Lifegroup?</h5>
              <p>Anyone can join a lifegroup who wants to know more about God, to understand the bible, and to grow together spiritually  </p>
            </div>
          </Col>
          <Col sm={12} lg={6}>
            <div className="young">
              <h5 className="lifegroup-header">Why join a Lifegroup?</h5>
            </div>
          </Col>
          <Col sm={12} lg={12}>
            <Button className="home-lifegroup-button" onClick={ () => setShowButton(true) }>
              click here to join
            </Button>
            <LifeGroupForm show={showButton} onHide={ () => setShowButton (false)} />
          </Col>
        </Row>
      </div>
    </section>

   </> 
  )
}

export default HeroSection;