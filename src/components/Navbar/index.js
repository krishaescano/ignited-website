import React, {useState, useEffect} from 'react';
import { FaBars } from 'react-icons/fa';
import { IconContext } from 'react-icons/lib';
// import { animateScroll as scroll } from 'react-scroll';
import { 
  Nav, 
  NavbarContainer, 
  NavLogo, 
  Image,
  MobileIcon, 
  MenuSpan,
  NavMenu, 
  NavItem, 
  NavLinks 
} from './NavbarElements';
import logo from '../../assets/images/logo.jpeg'

const Navbar = ({ toggle }) => {
  const [scrollNav, setScrollNav] = useState(false)

  //when I pass particular point, I want to trigger this nav
  const changeNav = () => {
    if(window.scrollY >= 80) {
      setScrollNav(true)
    } else {
      setScrollNav(false)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', changeNav)
  }, []);

  // const toggleHome = () => {
  //   scroll.scrollToTop();
  // }

  return (
    <>
    <IconContext.Provider value={{color: '#fff'}}>
      <Nav scrollNav={scrollNav}>
        <NavbarContainer>
          <NavLogo to='/'> 
          {/* onClick={toggleHome} */}
            <Image src={logo}/>
          </NavLogo>
          
          {/* hamburger menu when responsive mode */}
          <MobileIcon onClick={toggle}>
            <FaBars /> <MenuSpan>menu</MenuSpan>
          </MobileIcon>
          
          {/* Navbar contents */}
          <NavMenu>
            <NavItem>
              <NavLinks to='/about'>
              {/* smooth={true} duration={500} spy={true} exact='true' offset={-80} */}
              About Us</NavLinks>
            </NavItem>

            <NavItem>
              <NavLinks to='/watch'>Watch</NavLinks>
            </NavItem>

            <NavItem>
              <NavLinks to='/resources'>Resources</NavLinks>
            </NavItem>

            <NavItem>
              <NavLinks to='/memory-verse'>Memory Verse</NavLinks>
            </NavItem>

            <NavItem>
              <NavLinks to='/contact'>Contact Us</NavLinks>
            </NavItem>

            <NavItem>
              <NavLinks to='/give'>Give</NavLinks>
            </NavItem>
          </NavMenu>


        </NavbarContainer>
      </Nav>
    </IconContext.Provider>
    </>
  )
}

export default Navbar;