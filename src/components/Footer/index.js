import React from 'react';
import { FooterContainer, FooterWrap, FooterLinksContainer, SocialWebsiteWrap, SocialMediaWrap,SocialIconsHeading, ChurchName, WebsiteRights, SocialIcons, SocialIconLink } from './FooterElements';
import { FaFacebook, FaYoutube, FaSpotify } from 'react-icons/fa';

const Footer = () => {
  return (
    <FooterContainer>
      <FooterWrap>
        <FooterLinksContainer>
          <SocialMediaWrap>

            <SocialIconsHeading>connect with us</SocialIconsHeading>
            <SocialIcons>
              <SocialIconLink href='//www.facebook.com/gifuchristianassembly' target='_blank' aria-label='Facebook'>
                <FaFacebook />
              </SocialIconLink>

              <SocialIconLink href='//www.youtube.com/channel/UCvBXrnv5c_ETmPuGqWn0-3g' target='_blank' aria-label='Youtube'>
                <FaYoutube />
              </SocialIconLink>

              <SocialIconLink href='//www.spotify.com/jp/' target='_blank' aria-label='Spotify'>
                <FaSpotify />
              </SocialIconLink>
            </SocialIcons>

            <SocialWebsiteWrap>
              <ChurchName>ignited</ChurchName>
              <WebsiteRights> &copy; {new Date().getFullYear()} All Rights Reserved. </WebsiteRights>
            </SocialWebsiteWrap>

          </SocialMediaWrap>
        </FooterLinksContainer>
      </FooterWrap>
    </FooterContainer>
  )
}

export default Footer;
