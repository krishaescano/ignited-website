import React from 'react';
import { SidebarContainer, Icon, CloseIcon, SidebarWrapper, SidebarMenu, SidebarLink } from './SidebarElements';

const Sidebar = ({ isOpen, toggle }) => {
  return (
    <SidebarContainer isOpen={isOpen} onClick={toggle}>
      <Icon onClick={toggle}>
        <CloseIcon />
      </Icon>

      <SidebarWrapper>
        <SidebarMenu>
          <SidebarLink to='/about' onClick={toggle}>
            About Us
          </SidebarLink>

          <SidebarLink to='/watch' onClick={toggle}>Watch</SidebarLink>

          <SidebarLink to='/resources' onClick={toggle}>Resources</SidebarLink>

          <SidebarLink to='/memory-verse' onClick={toggle}>Memory Verse</SidebarLink>

          <SidebarLink to='/contact' onClick={toggle}>Contact Us</SidebarLink>

          <SidebarLink to='/give' onClick={toggle}>Give</SidebarLink>
        </SidebarMenu>

      </SidebarWrapper>

    </SidebarContainer>
  )
}

export default Sidebar;