import React from 'react';
import { Row, Col, Form, Modal } from 'react-bootstrap';
import { Button } from '../ButtonElements';

const GiveForm = (props) => {
  

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className="give-contents"
    >
      <Modal.Header className="give-heading2" closeButton>
        <Modal.Title className="giveFormHeading" id="contained-modal-title-vcenter">
          ATM/Debit Card/Credit Card
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form action="mailto:krishamayramos22@gmail.com" method=""> {/* onSubmit={handleSubmit}>*/}
              <Row>
                <Col sm={12} lg={4} className="form-name">
                  <Form.Group controlId="" className="give-form-label">
                    <Form.Label>Select branch</Form.Label>
                    <Form.Control as="select" className="input-area">
                      <option>Choose...</option>
                      <option>Gifu</option>
                      <option>Nagoya</option>
                      <option>others</option>
                    </Form.Control>
                  </Form.Group>
                </Col>

                <Col sm={12} lg={4} className="form-name">
                  <Form.Group controlId="" className="give-form-label">
                    <Form.Label>Select type/s of giving</Form.Label>
                    <Form.Control as="select" className="input-area">
                      <option>Choose...</option>
                      <option>tithes</option>
                      <option>offering</option>
                      <option>missions</option>
                      <option>building funds</option>
                      <option>others</option>
                    </Form.Control>
                  </Form.Group>
                </Col>

                <Col sm={12} lg={4} className="form-name">
                  <Form.Group className="give-form-label">
                    Amount
                   <Form.Control className="input-area" type="number" name="" placeholder="0.00" required/>
                   </Form.Group>
                </Col>           

                <Col lg={12}>
                  <Form.Group className="give-form-label">      
                    Card Number  
                    <Form.Control className="input-area" type="text" name="" placeholder="CARD NUMBER" maxlength="16" required/>
                  </Form.Group>            
                </Col> 

                <Col lg={6}>
                  <Form.Group className="give-form-label">      
                    Expiration  
                    <Form.Control className="input-area" type="text" name="" placeholder="EXPIRY (MM-YYYY)" maxlength="7" required/>
                  </Form.Group>            
                </Col> 

                <Col lg={6}>
                  <Form.Group className="give-form-label">      
                    CVV 
                    <Form.Control className="input-area" type="email" name="" placeholder="CVV" maxlength="4" required/>
                  </Form.Group>            
                </Col>  

                <Col lg={12}>
                  <Form.Group className="give-form-label">      
                    Name on Card 
                    <Form.Control className="input-area" type="email" name="" placeholder="NAME ON CARD" maxlength="60" required/>
                  </Form.Group>            
                </Col>      

                <Col lg={6}>
                  <Form.Group className="give-form-label">      
                    Email Address
                    <Form.Control className="input-area" type="email" name="email" placeholder="email@example.com" required/>
                  </Form.Group>            
                </Col> 

                <Col lg={6}>
                  <Form.Group className="give-form-label">      
                    Phone Number  
                    <Form.Control className="input-area" type="tel" name="email" placeholder="08012345678" required/>
                  </Form.Group>            
                </Col>

                <Col lg={12}>      
                  <Form.Group className="giveformButton-wrap">      
                    <Button type="submit" className="giveFormButton" /*onClick={thankYou}*/>Give</Button>
                  </Form.Group>                     
                </Col>     
              </Row>            
            </Form>
      </Modal.Body>
    </Modal>
  )
}

export default GiveForm
