import React, {useState} from 'react';
import '../assets/css/App.css';
import { Container, Col, Row, Modal } from 'react-bootstrap';
import img from '../assets/images/memory-verse.jpg';
import imglogo from '../assets/images/memory-verse-pic.png';
import { FaEye, FaDownload } from 'react-icons/fa';
import Footer from '../components/Footer';

const MemoryVerse = () => {
  const [showJan, setShowJan] = useState(false);
  const [showFeb, setShowFeb] = useState(false);
  const [showMarch, setShowMarch] = useState(false);
  const [showApril, setShowApril] = useState(false);

  const handleCloseJan = () => setShowJan(false);
  const handleShowJan = () => setShowJan(true);

  const handleCloseFeb = () => setShowFeb(false);
  const handleShowFeb = () => setShowFeb(true);
  
  const handleCloseMarch = () => setShowMarch(false);
  const handleShowMarch = () => setShowMarch(true);

  const handleCloseApril = () => setShowApril(false);
  const handleShowApril = () => setShowApril(true);

  return (
    <>
      <section>
        <div className="memory-verse-heading">
            <img className="memory-verse-img" src={img} alt='memory-verse-img' />
          <Row className="description-mem-verse">
            <Col sm={12} lg={6} data-aos="zoom-in">
              <img src={imglogo} alt='memory-verse-pic' className="logo-memory-verse" /> 
            </Col>
            <Col sm={12} lg={6}>
              <h5 data-aos="fade-down" className="memory-verse-h5">Then He said to them, &quot;Thus it is written, and thus it was necessary for the Christ to suffer and to rise from the dead the third day, and that repentance and remission of sins should be preached in His name to all nations, beginning at Jerusalem&quot;.
              <p className="memory-verse-p">Luke 24:46-47</p>
              </h5>
              
            </Col>
          </Row>
        </div>
      </section>

      <section className="memory-verse">
        <Row>
          <Col>
            <h1>{new Date().getFullYear()}</h1>
          <Container>
            <Row className="verse-row">
              <Col sm={12} md={6} lg={3}><strong>January</strong></Col>
              <Col sm={12} md={6} lg={3}>John 1:1-2</Col>
              <Col xs={6} sm={6} md={6} lg={3}>
                <Modal show={showJan} onHide={handleCloseJan} centered>
                  <Modal.Header closeButton>
                    <Modal.Title>John 1:1-2</Modal.Title>
                  </Modal.Header>
       
                  <Modal.Body>
                  In the beginning was the Word, and the Word was God. He was in the beginning with God.
                  </Modal.Body>
                </Modal>
                <p className="text-uppercase" onClick={handleShowJan}>
                  <FaEye/> view
                </p>
              </Col>
              <Col xs={6} sm={6} md={6} lg={3}>
                <a className="text-uppercase" href='https://drive.google.com/file/d/1Fr5FvEFJ982-GfrJMntoGyPQ5erlK6Ni/view?usp=sharing' target='_blank' aria-label='january-memory-verse' download> <FaDownload/>download</a>
              </Col>
            </Row>

            <Row className="verse-row">
              <Col sm={12} md={6} lg={3}><strong>February</strong></Col>
              <Col sm={12} md={6} lg={3}>Hebrews 10:38-39</Col>
              <Col xs={6} sm={6} md={6} lg={3}>
                <Modal show={showFeb} onHide={handleCloseFeb} centered>
                  <Modal.Header closeButton>
                    <Modal.Title>Hebrews 10:38-39</Modal.Title>
                  </Modal.Header>
       
                  <Modal.Body>
                  And Now the just shall live by faith but if anyone draws back, My soul has no pleasure in him.  But we are not of those who draws back to perdition, but of those who believe to the saving of the soul.
                  </Modal.Body>
                </Modal>
                <p onClick={handleShowFeb}>
                  <FaEye/> <span className="text-uppercase">view</span>
                </p>
              </Col>
              <Col xs={6} sm={6} md={6} lg={3}>
                <a className="text-uppercase" href='../assets/images/january-memory-verse.jpeg' target='_blank' aria-label='february-memory-verse' download> <FaDownload/>download</a>
              </Col>
            </Row>

            <Row className="verse-row">
              <Col sm={12} md={6} lg={3}><strong>March</strong></Col>
              <Col sm={12} md={6} lg={3}>Psalms 37:18-19</Col>
              <Col xs={6} sm={6} md={6} lg={3}>
                <Modal show={showMarch} onHide={handleCloseMarch} centered>
                  <Modal.Header closeButton>
                    <Modal.Title>Psalms 37:18-19</Modal.Title>
                  </Modal.Header>
       
                  <Modal.Body>
                  The Lord knows the days of the upright, and their inheritance shall be forever. They shall not be ashamed in the evil time, and in the days of famine they shall be satisfied.
                  </Modal.Body>
                </Modal>
                <p onClick={handleShowMarch}>
                  <FaEye/> <span className="text-uppercase">view</span>
                </p>
              </Col>
              <Col xs={6} sm={6} md={6} lg={3}>
                <a className="text-uppercase" href='../assets/images/january-memory-verse.jpeg' target='_blank' aria-label='march-memory-verse' download> <FaDownload/>download</a>
              </Col>
            </Row>

            <Row className="verse-row">
              <Col sm={12} md={6} lg={3}><strong>April</strong></Col>
              <Col sm={12} md={6} lg={3}>Luke 24:46-47</Col>
              <Col xs={6} sm={6} md={6} lg={3}>
                <Modal show={showApril} onHide={handleCloseApril} centered>
                  <Modal.Header closeButton>
                    <Modal.Title>Luke 24:46-47</Modal.Title>
                  </Modal.Header>
       
                  <Modal.Body>
                  Then He said to them, "Thus it is written, and thus it was necessary for the Christ to suffer and to rise from the dead the third day, and that repentance and remission of sins should be preached in His name to all nations, beginning at Jerusalem"
                  </Modal.Body>
                </Modal>
                <p onClick={handleShowApril}>
                  <FaEye/> <span className="text-uppercase">view</span>
                </p>
              </Col>
              <Col xs={6} sm={6} md={6} lg={3}>
                <a className="text-uppercase" href='../assets/images/january-memory-verse.jpeg' target='_blank' aria-label='april-memory-verse' download> <FaDownload/>download</a>
              </Col>
            </Row>

          </Container>
          </Col>
        </Row>
      </section>
      
      <Footer />
    </>
  )
}

export default MemoryVerse;
