import React from 'react';
import '../assets/css/App.css';
import { Container, Row, Col } from 'react-bootstrap';
import img from '../assets/images/resources.jpg';
import Image1 from '../assets/images/youth.svg';
import Image2 from '../assets/images/men.svg';
import Image3 from '../assets/images/women.svg';
import { FaYoutube, FaSpotify, FaDownload } from 'react-icons/fa';
import { Button } from '../components/ButtonElements';
import Footer from '../components/Footer';

const Resources = () => {
  return (
    <>
    <section>
        <div className="resources-heading">
          <img className="resources-img" src={img} alt='resources-img' />
            <h1 data-aos="flip-up" className="resources-description">Resources</h1>
        </div>
      </section>
      
      <section>
        <Container>
        <Row>
          <Col>
            <Row>
              <Col className="lifegroup-wrapper">
                <h3>Sunday Message</h3>
                <div>
                  <p>
                    <a className="sunday-message-resources" target="_blank" href="/watch" alt="watch-sunday-message"><FaYoutube /> Watch</a> 
                    <code className="codebar">&#124;</code>
                    <a className="sunday-message-resources" target="_blank" rel="noreferrer" href="https://www.spotify.com/jp/" alt="listen-sunday-message"><FaSpotify /> Listen</a>
                    <code className="codebar">&#124;</code>
                    <a className="sunday-message-resources" target="_blank" href="./../assets/files/week1-mystory.pdf" alt="download-sunday-message" download><FaDownload/> Download</a>
                  </p>
                </div>
              </Col>
            </Row>
            <Row>
              <Col className="lifegroup-wrapper">
                <h3>Lifegroup</h3>
                
                  <Row>
                    <Col sm={12} lg={4} className="resources-card-wrapper">
                      <div className="resources-card">
                          <img className="resources-image" src={Image1} alt="youth-img"/>
                          <h5 className="resources-h5">Youth</h5>
                          <Button className="lifegroup-view-button"><a className="lifegroup-button-text" target="_blank" href="">View</a></Button>
                      </div>
                    </Col>
                    <Col sm={12} lg={4} className="resources-card-wrapper">
                      <div className="resources-card">
                          <img className="resources-image" src={Image2} alt="men-img"/>
                          <h5 className="resources-h5">Men</h5>
                          <Button className="lifegroup-view-button"><a className="lifegroup-button-text" target="_blank" href="">View</a></Button>
                      </div>
                    </Col>
                    <Col sm={12} lg={4} className="resources-card-wrapper">
                      <div className="resources-card">
                          <img className="resources-image" src={Image3} alt="women-img"/>
                          <h5 className="resources-h5">Women</h5>
                          <Button className="lifegroup-view-button"><a className="lifegroup-button-text" target="_blank" href="../components/ResourcesData/WomenResources">View</a></Button>
                      </div>
                    </Col>
                  </Row>
                  
              </Col>
            </Row>
          </Col>
        </Row>
        </Container>
      </section>
      <Footer />
    </>
  )
}

export default Resources;
