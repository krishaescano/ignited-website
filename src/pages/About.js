import React from 'react';
import '../assets/css/App.css';
import { Container, Row, Col, Accordion, Card, Table } from 'react-bootstrap';
import img from '../assets/images/about3.jpeg';
import Footer from '../components/Footer';

const About = () => {
  return (
    <>
      <section>
        <div className="about-heading">
          <img className="about-img" src={img} alt='about-img' />
            <h1 data-aos="fade-right" className="about-description">About</h1>
        </div>
      </section>

      <section className="about">
        <Container>
          <h1>Welcome to Ignited Christian Assembly International</h1>
          <Row>
            <Col sm={12} lg={6}>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Soluta corporis recusandae sed odio fugiat et placeat, in a dolore. Quis ipsa facere dolorem dolorum possimus eligendi obcaecati exercitationem porro accusamus? Molestiae ipsam, ducimus porro sit, quia, amet non soluta quam error fugit laborum reprehenderit sint! Incidunt ipsa similique veritatis soluta facilis numquam nulla amet! Quis odit minima architecto, perspiciatis officiis quisquam est necessitatibus. Sapiente odio magni possimus facere iste ea inventore illum quidem molestias earum qui adipisci iure impedit id, dicta ad fugiat harum quo voluptatibus excepturi quae mollitia totam perferendis. Consequuntur nam laboriosam, soluta optio rerum blanditiis dolore labore.
            </Col>
            <Col sm={12} lg={6}>
              <Accordion>
                <Card>
                  <Accordion.Toggle as={Card.Header} eventKey="0">
                    History
                  </Accordion.Toggle>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>IGNITED CHRISTIAN ASSEMBLY INTERNATIONAL which was previously named Gifu Christian Assembly, was founded and organized in 2005 by Reverend Julio Pasiteng Balway who was also installed as the Senior Pastor until the present.  In 2014, Gifu Christian Assembly was situated to its current site in 984 Sakakura, Sakahogi, Kamo District, Gifu, to accommodate its growing membership; and  in 2020 the name of the church was changed from Gifu Christian Assembly to Ignited Christian Assembly International, to reflect the increasing membership in places other than Gifu, and international composition of the Church which currently includes two churches in Baguio City, Philippines and one church in Mainit, Bontoc Mountain Province, Philippines.</Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <Accordion.Toggle as={Card.Header} eventKey="1">
                    Our Mission
                  </Accordion.Toggle>
                  <Accordion.Collapse eventKey="1">
                    <Card.Body>The Mission of IGNITED CHRISTIAN ASSEMBLY INTERNATIONAL is to win souls for Christ; disciple & develop them to be ignited for the Lord and equip them for the Christian service so that God`s name will be glorified and exalted.</Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <Accordion.Toggle as={Card.Header} eventKey="2">
                    Our Vision
                  </Accordion.Toggle>
                  <Accordion.Collapse eventKey="2">
                    <Card.Body>The Vision of IGNITED CHRISTIAN ASSEMBLY INTERNATIONAL is to be an Inspired, Growing, Notable, Intense, Transformed, Empowered and Dedicated church.</Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <Accordion.Toggle as={Card.Header} eventKey="3">
                    Our Core Values
                  </Accordion.Toggle>
                  <Accordion.Collapse eventKey="3">
                    <Card.Body>Priority:   Loving God is first priority, second is family and third is ministry.   
                    Conduct:  We give utmost importance to maintaining integrity in every area whether privately or publicly.    
                    Outcome:   We strive for excellence in everything we do for God’s glory.
                    Fruit:  We are dedicated to bearing fruit worthy as God’s disciples. </Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <Accordion.Toggle as={Card.Header} eventKey="4">
                    Our Statement of Faith
                  </Accordion.Toggle>
                  <Accordion.Collapse eventKey="4">
                    <Card.Body>
                      <div>
                        <h6>What does ignited believe about the bible?</h6>
                        <p>The Scriptures both the Old and New Testaments are verbally inspired of God and are the revelation of God to man, the infallible, authoritative rule of faith and conduct (2 Timothy 3:15-17; 1 Thessalonians 2:13; 2 Peter 1:21).</p>
                        <h6>What does ignited believe about God?</h6>
                        <p>The one true God has revealed Himself as the eternally self-existent “I AM,” the Creator of heaven and earth and the Redeemer of mankind. He has further revealed Himself as embodying the principles of relationship and association as Father, Son, and Holy Spirit (Deuteronomy 6:4; Isaiah 43:10, 11; Matthew 28:19; Luke 3:22).</p>
                        <h6>What does ignited believe about Jesus Christ?</h6>
                        <p>The Lord Jesus Christ is the eternal Son of God. The Scriptures declare:

His virgin birth (Matthew 1:23; Luke 1:31, 35).
His sinless life (Hebrews 7:26; 1 Peter 2:22).
His miracles (Acts 2:22; 10:38).
His substitutionary work on the cross (1 Corinthians 15:3; 2 Corinthians 5:21).
His bodily resurrection from the dead (Matthew 28:6; Luke 24:39; 1 Corinthians 15:4)
His exaltation to the right hand of God (Acts 1:9, 11; 2:33; Philippians 2:9-11; Hebrews 1:3).</p>
                        <h6>What does ignited believe about Salvation of Man?</h6>
                        <p>Man’s only hope of redemption is through the shed blood of Jesus Christ the Son of God.

Conditions to Salvation. Salvation is received through repentance toward God and faith toward the Lord Jesus Christ. By the washing of regeneration and renewing of the Holy Spirit, being justified by grace through faith, man becomes an heir of God according to the hope of eternal life (Luke 24:47; John 3:3; Romans 10:13-15; Ephesians 2:8; Titus 2:11; 3:5-7).
The Evidences of Salvation. The inward evidence of salvation is the direct witness of the Spirit (Romans 8:16). The outward evidence to all men is a life of righteousness and true holiness (Ephesians 4:24; Titus 2:12).</p>
                        <h6>What does ignited believe about Baptism of the Holy Spirit? </h6>
                        <p>All believers are entitled to and should ardently expect and earnestly seek the promise of the Father, the baptism in the Holy Spirit and fire, according to the command of our Lord Jesus Christ. This was the normal experience of all in the early Christian Church. With it comes the enduement of power for life and service, the bestowment of the gifts and their uses in the work of the ministry (Luke 24:49; Acts 1:4, 8; 1 Corinthians 12:1-31).  This experience is distinct from and subsequent to the experience of the new birth (Acts 8:12-17; 10:44-46; 11:14-16; 15:7-9). With the baptism in the Holy Spirit come such experiences as an overflowing fullness of the Spirit (John 7:37-39; Acts 4:8), a deepened reverence for God (Acts 2:43; Hebrews 12:28), an intensified consecration to God and dedication to His work (Acts 2:42), and a more active love for Christ, for His Word, and for the lost (Mark 16:20).</p>
                        <h6>What does ignited believe about Sanctification? </h6>
                        <p>Sanctification is an act of separation from that which is evil, and of dedication unto God (Romans 12:1, 2; 1 Thessalonians 5:23; Hebrews 13:12). The Scriptures teach a life of “holiness without which no man shall see the Lord” (Hebrews 12:14). By the power of the Holy Spirit we are able to obey the command: “Be ye holy, for I am holy” (1 Peter 1:15, 16).

Sanctification is realized in the believer by recognizing the identification with Christ in His death and resurrection, and by faith reckoning daily upon the fact of that union, and by offering every faculty continually to the dominion of the Holy Spirit (Romans 6: 1-11, 13; 8:1, 2, 13; Galatians 2:20; Philippians 2:12, 13; 1 Peter 1:5).</p>
                        <h6>To know more about ignited Statement of Faith </h6>
                        <p><a href="http://www.pgcag.com/site/home-2/beliefs/what-we-believe/?fbclid=IwAR2epa781YxX0sxaXjBegyuMbrLGy0l1-2mVuWd-lRRt_xJEir-qrYXCXsQ" target="_blank" rel="noreferrer" alt="statement-of-faith-link">click here </a></p>
                      </div>
                    
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            </Col>
            <Col lg={12}>
                <h2>Locations</h2>
              <Container className="locations-container">
                <Row className="locations-row">
                  <Col className="locations-col">
                    <Row className="locations-wrap">
                      <Col className="location-main" sm={12} lg={12}>
                        <Row>
                        <Col sm={12} lg={6}>
                          <h3>Ignited Gifu - Main</h3>
                          <Table>
                            <tr>
                              <td>Address: </td>
                              <td>984 Sakahogi Cho Sakakura, Kamogun, Gifu Ken, Japan 505-0074</td>
                            </tr>
                            <tr>
                              <td>Phone: </td>
                              <td>(+81)8016053375</td>
                            </tr>
                            <tr>
                              <td>Service:</td>
                              <td>Sundays: 10:00am to 12:30pm</td>
                            </tr>
                          </Table>
                          <div>
                            <p>
                            <a className="map-details" target="_blank" rel="noreferrer" href="https://goo.gl/maps/aRTfHiuS7gh1M6xEA" alt="ignited-gifu-fullmap">View Full Map</a> 
                            <code className="codebar">&#124;</code>
                            <a className="map-details" target="_blank" rel="noreferrer" href="https://goo.gl/maps/uGMNZAPT8BfH5A736" alt="direction-to-ignited-gifu">Directions</a>
                            </p>
                          </div>
                        </Col>
                        <Col sm={12} lg={6}>
                          <div>
                          <iframe title="Ignited-Gifu-Church-Map" className="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d900.8347031184131!2d137.00044400994582!3d35.437993313633136!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x600310481801a1c9%3A0xd3390877ad38b3d8!2sGifu%20Christian%20Assembly%20Church!5e1!3m2!1sen!2sjp!4v1615442253908!5m2!1sen!2sjp"  allowfullscreen="" loading="lazy"></iframe>
                          </div>
                        </Col>
                        </Row>
                      </Col>
                      <Col className="location1" sm={12} lg={12}>
                        <Row>
                          <Col sm={12} lg={6}>
                            <h3>Ignited Nagoya</h3>
                            <Table>
                              <tr>
                                <td>Address: </td>
                                <td>Will Aichi Building, 1 Kamitatesugino, Higashi Ku, Nagoya Shi, Aichi 461-0016</td>
                              </tr>
                              <tr>
                                <td>Phone: </td>
                                <td>(+81)8016053375</td>
                              </tr>
                              <tr>
                                <td>Service:</td>
                                <td>Sundays: 5:00pm to 7:30pm</td>
                              </tr>
                            </Table>
                            <div>
                              <p>
                              <a className="map-details" target="_blank" rel="noreferrer" href="https://goo.gl/maps/BDWopEkeDsaAuRui7" alt="ignited-aichi-fullmap">View Full Map</a> 
                              <code className="codebar">&#124;</code>
                              <a className="map-details" target="_blank" rel="noreferrer" href="https://goo.gl/maps/zpabsxVzhmLJupKS6" alt="direction-to-ignited-aichi">Directions</a>
                              </p>
                            </div>
                          </Col>
                          <Col sm={12} lg={6}>
                            <div>
                              <iframe title="Ignited-Nagoya-Church-Map" className="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5675.853613664228!2d136.9085457850594!3d35.17869659250765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xac2f9dd60492ffaa!2sWill%20Aichi!5e1!3m2!1sen!2sjp!4v1615444985658!5m2!1sen!2sjp" allowfullscreen="" loading="lazy"></iframe>
                            </div>

                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Container>
            </Col>
          </Row>
        </Container>
      </section>
      <Footer />
    </>
  )
}

export default About;
