import React from 'react';
import '../assets/css/App.css';
import { Container, Row, Col, Form } from 'react-bootstrap';
import img from '../assets/images/about2.jpeg';
import { Button } from '../components/ButtonElements';
import Footer from '../components/Footer';

const Contact = () => {
  return (
    <>
    <section>
      <div className="contact-heading">
        <img className="contact-img" src={img} alt='contact-img' />
        <h1 data-aos="fade-up-left" className="contact-description">Contact</h1>
      </div>
    </section>

    <section className="contact-contents">
      <Container>
        <Row>
          <Col className="contact-heading2">
          <h1>We would love to hear from you!</h1>
          <p>Do you have feedbacks? inquiries? questions? need someone to talk to?</p>
          </Col>
        </Row>
        <Row>
          <Col sm={12} lg={6}>
            <Container>
            <h4>Send us a Message</h4>
            <Form className="contact-form" action="mailto:krishamayramos22@gmail.com" method="">
              <Row>
                <Col lg={12} className="form-name">
                  <Form.Group className="form-label">
                    Full Name
                   <Form.Control className="input-area" type="text" name="fullName" placeholder="John Doe" required/>
                   </Form.Group>
                </Col>

                <Col lg={12}>
                  <Form.Group className="form-label">      
                    Email Address
                    <Form.Control className="input-area" type="email" name="email" placeholder="email@example.com" required/>
                  </Form.Group>            
                </Col>            

                <Col lg={12}>      
                  <Form.Group className="form-label">      
                    Message   
                    <Form.Control as="textarea" rows="5" cols="20" placeholder="Your Message Here" required/>
                  </Form.Group>            
                </Col>      

                <Col lg={12}>      
                  <Form.Group>      
                    <Button type="submit" className="submitButton">Submit</Button>
                  </Form.Group>                     
                </Col>      
                <Col md={1} lg={1}></Col>      
              </Row>            
            </Form>
            </Container>
          </Col>
          <Col sm={12} lg={6}>
            <Container>
              <h4>Our Address:</h4>
              <p>984 Sakahogi Cho Sakakura, Kamogun, Gifu Ken, Japan 505-0074</p>
              <div>
                <iframe title="Ignited-Gifu-Church-Map" className="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d900.8347031184131!2d137.00044400994582!3d35.437993313633136!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x600310481801a1c9%3A0xd3390877ad38b3d8!2sGifu%20Christian%20Assembly%20Church!5e1!3m2!1sen!2sjp!4v1615442253908!5m2!1sen!2sjp"  allowfullscreen="" loading="lazy"></iframe>
              </div>
              <div>
                <h4>Email or Call us</h4>
                <p>ignitedcai@yahoo.com <code className="codebar">&#124;</code> (+81)8016053375
                </p>
              </div>
            </Container>
          </Col>
        </Row>
      </Container>
    </section>

    <Footer />    
    </>
  )
}

export default Contact;
