import React, { useState} from 'react';
import img from '../assets/images/give2.jpeg';
import { Button } from '../components/ButtonElements'
import GiveForm from '../components/GiveComponents/GiveForm';
import Footer from '../components/Footer';
const Give = () => {
  const [ showButton, setShowButton ] = useState(false);

  return (
    <>
    <section>
      <div className="give-heading">
        <img className="give-img" src={img} alt='give-img' />
        <p className="give-description">&quot;Whoever sows sparingly will also reap sparingly, and whoever sows bountifully will also reap bountifully. Each one must give as he has decided in his heart, not reluctantly or under compulsion, for God loves a cheerful giver. And God is able to make all grace abound to you, so that having all sufficiency in all things at all times, you may abound in every good work.&quot;
        <p>2 Corinthians 9:6-8</p>

        <Button className="givepage-button" onClick={ () => setShowButton(true) }>
          Give
        </Button>
        <GiveForm show={showButton} onHide={ () => setShowButton (false)} />
        
        </p>
      </div>
    </section>
    <Footer/>
    </>
  )
}

export default Give;
