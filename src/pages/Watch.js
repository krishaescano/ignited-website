import React from 'react';
import '../assets/css/App.css';
import img from '../assets/images/watch.jpg';
import { Button } from '../components/ButtonElements';
import Footer from '../components/Footer';

const Watch = () => {
  return (
    <>
      <section>
        <div className="watch-banner">
          <img className="watch-img" src={img} alt='watch-img' />
          <h1 data-aos="fade-left" className="h1-watch">Watch</h1>
        </div>
      </section>

      <section className="text-wrapper">
        <div className="description">
          <p>Have a Question? Joining Lifegroup? Need Prayer?</p>
          <Button className="watch-button-wrap">
            <a className="watch-button" href='https://www.facebook.com/messages/t/1046947165420898' target='_blank' rel="noreferrer" aria-label='messenger'>Chat with us</a>
          </Button>
        </div>
      </section>
      
      <section className="watch-live">
        <div>
          <div className="fb-video" data-href="https://www.facebook.com/gifuchristianassembly/videos/272860674323014/" data-width="1000" data-show-text="false" data-allowfullscreen="true">
            <blockquote cite="https://developers.facebook.com/gifuchristianassembly/videos/272860674323014/" className="fb-xfbml-parse-ignore">
            <a href="https://developers.facebook.com/gifuchristianassembly/videos/272860674323014/">April 4, 2021 Sunday Service</a>
            <p></p>Posted by <a href="https://www.facebook.com/gifuchristianassembly/">Ignited Christian Assembly International</a> on Saturday, April 3, 2021
            </blockquote>
          </div>
        </div>

        <div className="video-description">
          <p>You can also hear the sunday message through <strong><a className="spotify-link" href='https://www.spotify.com/jp/' target='_blank' rel="noreferrer" aria-label='spotify'>Spotify</a></strong></p>
        </div>
      </section>
      <Footer />
    </>
  )
}

export default Watch;