import React, { useState} from 'react';
import './assets/css/App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './pages';
import About from './pages/About';
import Contact from './pages/Contact';
import MemoryVerse from './pages/MemoryVerse';
import Resources from './pages/Resources';
import Watch from './pages/Watch';
import Navbar from './components/Navbar';
import Sidebar from './components/Sidebar';
import Give from './pages/Give';


function App() {
  const [ isOpen, setIsOpen ] = useState(false)
  // update the state and reverse it
  //set the state to true and then back to false when true
  const toggle = () => {
    setIsOpen(!isOpen)
  };
  return (
    <BrowserRouter>
      <Sidebar isOpen={isOpen} toggle={toggle}/>
      <Navbar toggle={toggle}/>
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/about" component={About} exact />
        <Route path="/watch" component={Watch} exact />
        <Route path="/resources" component={Resources} exact />
        <Route path="/memory-verse" component={MemoryVerse} exact />
        <Route path="/contact" component={Contact} exact />
        <Route path="/give" component={Give} exact />

      </Switch>
    </BrowserRouter>
  )
}

export default App;